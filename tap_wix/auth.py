import json
from datetime import datetime
from typing import Optional

import requests
from singer_sdk.authenticators import APIAuthenticatorBase
from singer_sdk.streams import Stream as RESTStreamBase


class Authenticator(APIAuthenticatorBase):
    def __init__(
        self,
        stream: RESTStreamBase,
        config_file: Optional[str] = None,
        auth_endpoint: Optional[str] = None,
    ) -> None:
        super().__init__(stream=stream)
        self._auth_endpoint = auth_endpoint
        self._config_file = config_file
        self._tap = stream._tap
        self.last_refresh = config_file.get("last_refresh")
        self.access_token = config_file.get("access_token")
        self.refresh_token = config_file.get("refresh_token")

    @property
    def auth_headers(self) -> dict:
        result = {}
        result["Content-Type"] = "application/json"
        if not self.is_token_valid():
            self.update_access_token()
        result["Authorization"] = f"{self.access_token}"
        return result

    @property
    def oauth_request_body(self) -> dict:
        return {
            "grant_type": "refresh_token",
            "client_id": self._tap._config["client_id"],
            "client_secret": self._tap._config["client_secret"],
            "refresh_token": self._tap._config["refresh_token"],
        }

    def is_token_valid(self) -> bool:

        if not self.last_refresh:
            return False

        if not self.access_token:
            return False

        now = round(datetime.utcnow().timestamp())

        return not ((self.last_refresh + 300) - now < 60)  # last 5 mins

    @property
    def auth_endpoint(self) -> str:
        if not self._auth_endpoint:
            raise ValueError("Authorization endpoint not set.")
        return self._auth_endpoint

    # Authentication and refresh
    def update_access_token(self) -> None:

        # prepares the request for the new token
        auth_request_payload = self.oauth_request_body
        token_response = requests.post(
            url=self.auth_endpoint, json=auth_request_payload
        )

        try:
            token_response.raise_for_status()
            self.logger.info("OAuth authorization attempt was successful.")
        except Exception as ex:
            raise RuntimeError(
                f"Failed OAuth login, response was '{token_response.json()}'. {ex}"
            )

        # saves new token into self.token
        token_json = token_response.json()
        self.access_token = token_json["access_token"]
        self.refresh_token = token_json["refresh_token"]

        # updates the config file
        self._tap._config["access_token"] = self.access_token
        self._tap._config["refresh_token"] = self.refresh_token
        self._tap._config["last_refresh"] = datetime.utcnow().timestamp()

        # writes the new token and the last login in the config file
        # for future use
        with open(self._tap.config_file, "w") as outfile:
            json.dump(self._tap._config, outfile, indent=4)
