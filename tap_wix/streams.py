"""Stream type classes for tap-wix."""

from singer_sdk import typing as th

from tap_wix.client import WixStream


class OrdersStream(WixStream):
    name = "orders"
    path = "/v2/orders/query"
    primary_keys = ["id"]
    records_jsonpath = "$.orders[*]"
    replication_key = "lastUpdated"
    filters = {"read": "true"}

    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("number", th.IntegerType),
        th.Property("dateCreated", th.DateTimeType),
        th.Property(
            "buyerInfo",
            th.ObjectType(
                th.Property("id", th.StringType),
                th.Property("type", th.StringType),
                th.Property("identityType", th.StringType),
                th.Property("firstName", th.StringType),
                th.Property("lastName", th.StringType),
                th.Property("phone", th.StringType),
                th.Property("email", th.StringType),
                th.Property("contactId", th.StringType),
            ),
        ),
        th.Property("currency", th.StringType),
        th.Property("weightUnit", th.StringType),
        th.Property(
            "totals",
            th.ObjectType(
                th.Property("subtotal", th.StringType),
                th.Property("shipping", th.StringType),
                th.Property("tax", th.StringType),
                th.Property("discount", th.StringType),
                th.Property("total", th.StringType),
                th.Property("weight", th.StringType),
                th.Property("quantity", th.IntegerType),
            ),
        ),
        th.Property(
            "billingInfo",
            th.ObjectType(
                th.Property("paymentMethod", th.StringType),
                th.Property("paymentGatewayTransactionId", th.StringType),
                th.Property(
                    "address",
                    th.ObjectType(
                        th.Property(
                            "fullName",
                            th.ObjectType(
                                th.Property("firstName", th.StringType),
                                th.Property("lastName", th.StringType),
                            ),
                        ),
                        th.Property("country", th.StringType),
                        th.Property("city", th.StringType),
                        th.Property("zipCode", th.StringType),
                        th.Property("phone", th.StringType),
                        th.Property("company", th.StringType),
                        th.Property("email", th.StringType),
                        th.Property("addressLine1", th.StringType),
                        th.Property("addressLine2", th.StringType),
                    ),
                ),
                th.Property("paidDate", th.DateTimeType),
            ),
        ),
        th.Property(
            "shippingInfo",
            th.ObjectType(
                th.Property("deliveryOption", th.StringType),
                th.Property("estimatedDeliveryTime", th.StringType),
                th.Property("shippingRegion", th.StringType),
                th.Property("deliverByDate", th.StringType),
                th.Property("code", th.StringType),
                th.Property("shipmentDetails",
                    th.ObjectType(
                        th.Property("address",
                            th.ObjectType(
                                th.Property(
                                    "fullName",
                                    th.ObjectType(
                                        th.Property("firstName", th.StringType),
                                        th.Property("lastName", th.StringType),
                                    ),
                                ),
                            th.Property("country", th.StringType),
                            th.Property("city", th.StringType),
                            th.Property("zipCode", th.StringType),
                            th.Property("phone", th.StringType),
                            th.Property("company", th.StringType),
                            th.Property("email", th.StringType),
                            th.Property("addressLine1", th.StringType),
                            th.Property("addressLine2", th.StringType),
                        ),  
                    ),
                    th.Property("trackingInfo",
                        th.ObjectType(
                            th.Property("trackingNumber", th.StringType),
                            th.Property("shippingProvider", th.StringType),
                            th.Property("trackingLink", th.StringType)
                        )
                    ),   
                    th.Property("discount", th.StringType),
                    th.Property("tax", th.StringType),
                    th.Property("priceData",
                        th.ObjectType(
                            th.Property("taxIncludedInPrice", th.BooleanType),
                            th.Property("price", th.StringType)
                        )
                    ),
                ),
            ),
        )),
        th.Property("read", th.BooleanType),
        th.Property("archived", th.BooleanType),
        th.Property("paymentStatus", th.StringType),
        th.Property("fulfillmentStatus", th.StringType),
        th.Property(
            "lineItems",
            th.ArrayType(
                th.ObjectType(
                    th.Property("index", th.IntegerType),
                    th.Property("quantity", th.IntegerType),
                    th.Property("price", th.StringType),
                    th.Property("name", th.StringType),
                    th.Property("totalPrice", th.StringType),
                    th.Property("lineItemType", th.StringType),
                    th.Property("options", th.ArrayType(th.CustomType({"type": ["object", "string"]}))),
                    th.Property("customTextFields", th.ArrayType(th.StringType)),
                    th.Property("notes", th.StringType),
                    th.Property("taxIncludedInPrice", th.BooleanType),
                    th.Property(
                        "priceDate",
                        th.ObjectType(
                            th.Property("taxIncludedInPrice", th.BooleanType),
                            th.Property("price", th.StringType),
                            th.Property("totalPrice", th.StringType),
                        ),
                    ),
                )
            ),
        ),
        th.Property(
            "activities",
            th.ArrayType(
                th.ObjectType(
                    th.Property("type", th.StringType),
                    th.Property("timestamp", th.DateTimeType),
                )
            ),
        ),
        th.Property("fulfillments", th.ArrayType(
            th.ObjectType(
                th.Property("id", th.StringType),
                th.Property("dateCreated", th.StringType),
                th.Property("lineItems", th.ArrayType(
                    th.ObjectType(
                        th.Property("index", th.IntegerType),
                        th.Property("quantity", th.IntegerType)
                    )
                )),
                th.Property("trackingInfo", th.ObjectType(
                    th.Property("trackingNumber", th.StringType),
                    th.Property("shippingProvider", th.StringType),
                    th.Property("trackingLink", th.StringType)
                ))
            )
        )),
        th.Property("buyerLanguage", th.StringType),
        th.Property("channelInfo", th.ObjectType(th.Property("type", th.StringType))),
        th.Property(
            "enteredBy",
            th.ObjectType(
                th.Property("id", th.StringType),
                th.Property("identityType", th.StringType),
            ),
        ),
        th.Property("lastUpdated", th.DateTimeType),
        th.Property("numericId", th.StringType),
        th.Property("refunds", th.ArrayType(th.ObjectType())),
        th.Property("isInternalOrderCreate", th.BooleanType),
    ).to_dict()


class AbandonedCartsStream(WixStream):
    name = "abandoned_carts"
    path = "/v1/abandonedCarts/query"
    primary_keys = ["id"]
    records_jsonpath = "$.abandonedCarts[*]"
    filters = {"status": "ABANDONED"}

    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("status", th.StringType),
        th.Property("abandonTime", th.DateTimeType),
        th.Property(
            "buyerInfo",
            th.ObjectType(
                th.Property("id", th.StringType),
                th.Property("identityType", th.StringType),
                th.Property("email", th.StringType),
                th.Property("firstName", th.StringType),
                th.Property("lastName", th.StringType),
            ),
        ),
        th.Property("total", th.StringType),
        th.Property(
            "activities",
            th.ArrayType(
                th.ObjectType(
                    th.Property("activityType", th.StringType),
                    th.Property("message", th.StringType),
                    th.Property("timestamp", th.DateTimeType),
                    th.Property("customData", th.CustomType({"type": ["object"]})),
                )
            ),
        ),
    ).to_dict()


class InventoryItemsStream(WixStream):
    name = "inventory_items"
    path = "/v2/inventoryItems/query"
    primary_keys = ["id"]
    records_jsonpath = "$.inventoryItems[*]"
    replication_key = "lastUpdated"

    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("externalId", th.StringType),
        th.Property("productId", th.StringType),
        th.Property("trackQuantity", th.BooleanType),
        th.Property(
            "variants",
            th.ArrayType(
                th.ObjectType(
                    th.Property("variantId", th.StringType),
                    th.Property("inStock", th.BooleanType),
                    th.Property("quantity", th.IntegerType),
                )
            ),
        ),
        th.Property("lastUpdated", th.DateTimeType),
        th.Property("numericId", th.StringType),
    ).to_dict()


class CatalogProductsStream(WixStream):
    name = "catalog_products"
    path = "/v1/products/query"
    primary_keys = ["id"]
    records_jsonpath = "$.products[*]"
    replication_key = "lastUpdated"

    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("name", th.StringType),
        th.Property("slug", th.StringType),
        th.Property("visible", th.BooleanType),
        th.Property("productType", th.StringType),
        th.Property("description", th.StringType),
        th.Property("sku", th.StringType),
        th.Property("weight", th.NumberType),
        th.Property(
            "weightRange",
            th.ObjectType(
                th.Property("minValue", th.NumberType),
                th.Property("maxValue", th.NumberType),
            ),
        ),
        th.Property(
            "stock",
            th.ObjectType(
                th.Property("trackInventory", th.BooleanType),
                th.Property("inStock", th.BooleanType),
                th.Property("trackStatus", th.StringType),
            ),
        ),
        th.Property(
            "price",
            th.ObjectType(
                th.Property("currency", th.StringType),
                th.Property("price", th.NumberType),
                th.Property(
                    "formatted",
                    th.ObjectType(
                        th.Property("price", th.StringType),
                        th.Property("discountedPrice", th.StringType),
                        th.Property("pricePerUnit", th.StringType),
                    ),
                ),
                th.Property("pricePerUnit", th.NumberType),
            ),
        ),
        th.Property(
            "priceData",
            th.ObjectType(
                th.Property("currency", th.StringType),
                th.Property("price", th.NumberType),
                th.Property(
                    "formatted",
                    th.ObjectType(
                        th.Property("price", th.StringType),
                        th.Property("discountedPrice", th.StringType),
                        th.Property("pricePerUnit", th.StringType),
                    ),
                ),
                th.Property("pricePerUnit", th.NumberType),
            ),
        ),
        th.Property(
            "convertedPriceData",
            th.ObjectType(
                th.Property("currency", th.StringType),
                th.Property("price", th.NumberType),
                th.Property(
                    "formatted",
                    th.ObjectType(
                        th.Property("price", th.StringType),
                        th.Property("discountedPrice", th.StringType),
                        th.Property("pricePerUnit", th.StringType),
                    ),
                ),
                th.Property("pricePerUnit", th.NumberType),
            ),
        ),
        th.Property(
            "priceRange",
            th.ObjectType(
                th.Property("minValue", th.NumberType),
                th.Property("maxValue", th.NumberType),
            ),
        ),
        th.Property(
            "costRange",
            th.ObjectType(
                th.Property("minValue", th.NumberType),
                th.Property("maxValue", th.NumberType),
            ),
        ),
        th.Property(
            "pricePerUnitData",
            th.ObjectType(
                th.Property("totalQuantity", th.NumberType),
                th.Property("totalMesurementUnit", th.StringType),
                th.Property("baseQuantity", th.NumberType),
                th.Property("baseMesurementUnit", th.StringType),
            ),
        ),
        th.Property(
            "additionalInfoSections",
            th.ArrayType(
                th.ObjectType(
                    th.Property("description", th.StringType),
                    th.Property("title", th.StringType),
                )
            ),
        ),
        th.Property(
            "ribbons",
            th.ArrayType(
                th.ObjectType(
                    th.Property("text", th.StringType),
                )
            ),
        ),
        th.Property(
            "media",
            th.ObjectType(
                th.Property(
                    "mainMedia",
                    th.ObjectType(
                        th.Property(
                            "thumbnail",
                            th.ObjectType(
                                th.Property("url", th.StringType),
                                th.Property("widtth", th.IntegerType),
                                th.Property("height", th.IntegerType),
                            ),
                        ),
                        th.Property("mediaType", th.StringType),
                        th.Property("title", th.StringType),
                        th.Property(
                            "image",
                            th.ObjectType(
                                th.Property("url", th.StringType),
                                th.Property("widtth", th.IntegerType),
                                th.Property("height", th.IntegerType),
                            ),
                        ),
                        th.Property("id", th.StringType),
                    ),
                ),
                th.Property(
                    "items",
                    th.ArrayType(
                        th.ObjectType(
                            th.Property(
                                "thumbnail",
                                th.ObjectType(
                                    th.Property("url", th.StringType),
                                    th.Property("widtth", th.IntegerType),
                                    th.Property("height", th.IntegerType),
                                ),
                            ),
                            th.Property("mediaType", th.StringType),
                            th.Property("title", th.StringType),
                            th.Property(
                                "image",
                                th.ObjectType(
                                    th.Property("url", th.StringType),
                                    th.Property("widtth", th.IntegerType),
                                    th.Property("height", th.IntegerType),
                                ),
                            ),
                            th.Property("id", th.StringType),
                        )
                    ),
                ),
            ),
        ),
        th.Property(
            "customTextFields",
            th.ArrayType(
                th.ObjectType(
                    th.Property("title", th.StringType),
                    th.Property("maxLength", th.NumberType),
                    th.Property("mandatory", th.BooleanType),
                )
            ),
        ),
        th.Property("manageVariants", th.BooleanType),
        th.Property(
            "productOptions",
            th.ArrayType(
                th.ObjectType(
                    th.Property("name", th.StringType),
                    th.Property("optionType", th.StringType),
                    th.Property(
                        "choices",
                        th.ArrayType(
                            th.ObjectType(
                                th.Property("value", th.StringType),
                                th.Property("visible", th.BooleanType),
                                th.Property("description", th.StringType),
                                th.Property("inStock", th.BooleanType),
                                th.Property(
                                    "media",
                                    th.ObjectType(
                                        th.Property(
                                            "items",
                                            th.ArrayType(
                                                th.ObjectType(
                                                    th.Property("id", th.StringType),
                                                    th.Property(
                                                        "mediaType", th.StringType
                                                    ),
                                                    th.Property("title", th.StringType),
                                                    th.Property(
                                                        "image",
                                                        th.ObjectType(
                                                            th.Property(
                                                                "height", th.IntegerType
                                                            ),
                                                            th.Property(
                                                                "url", th.StringType
                                                            ),
                                                            th.Property(
                                                                "width", th.IntegerType
                                                            ),
                                                        ),
                                                    ),
                                                    th.Property(
                                                        "thumbnail",
                                                        th.ObjectType(
                                                            th.Property(
                                                                "height", th.IntegerType
                                                            ),
                                                            th.Property(
                                                                "url", th.StringType
                                                            ),
                                                            th.Property(
                                                                "width", th.IntegerType
                                                            ),
                                                        ),
                                                    ),
                                                )
                                            ),
                                        ),
                                        th.Property(
                                            "mainMedia",
                                            th.ObjectType(
                                                th.Property("id", th.StringType),
                                                th.Property("mediaType", th.StringType),
                                                th.Property("title", th.StringType),
                                                th.Property(
                                                    "image",
                                                    th.ObjectType(
                                                        th.Property(
                                                            "height", th.IntegerType
                                                        ),
                                                        th.Property(
                                                            "url", th.StringType
                                                        ),
                                                        th.Property(
                                                            "width", th.IntegerType
                                                        ),
                                                    ),
                                                ),
                                                th.Property(
                                                    "thumbnail",
                                                    th.ObjectType(
                                                        th.Property(
                                                            "height", th.IntegerType
                                                        ),
                                                        th.Property(
                                                            "url", th.StringType
                                                        ),
                                                        th.Property(
                                                            "width", th.IntegerType
                                                        ),
                                                    ),
                                                ),
                                            ),
                                        ),
                                    ),
                                ),
                            )
                        ),
                    ),
                )
            ),
        ),
        th.Property(
            "productPageUrl",
            th.ObjectType(
                th.Property("base", th.StringType),
                th.Property("path", th.StringType),
            ),
        ),
        th.Property("numericId", th.StringType),
        th.Property("inventoryItemId", th.StringType),
        th.Property(
            "discount",
            th.ObjectType(
                th.Property("type", th.StringType),
                th.Property("value", th.NumberType),
            ),
        ),
        th.Property("collectionIds", th.ArrayType(th.StringType)),
        th.Property("variants", th.ArrayType(th.StringType)),
        th.Property("lastUpdated", th.DateTimeType),
        th.Property("createDate", th.DateTimeType),
        th.Property("ribbon", th.StringType),
    ).to_dict()
