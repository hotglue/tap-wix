import json
from typing import Any, Optional, cast

import requests
from dateutil.parser import parse
from singer_sdk.helpers.jsonpath import extract_jsonpath
from singer_sdk.streams import RESTStream

from tap_wix.auth import Authenticator


class WixStream(RESTStream):
    url_base = "https://www.wixapis.com/stores"
    auth_endpoint = "https://www.wixapis.com/oauth/access"
    _page_size = 100
    rest_method = "POST"
    filters = {}

    @property
    def authenticator(self) -> Authenticator:
        return Authenticator(self, self._tap.config, self.auth_endpoint)

    def prepare_request(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> requests.PreparedRequest:

        http_method = self.rest_method
        url: str = self.get_url(context)
        params: dict = self.get_url_params(context, next_page_token)
        request_data = json.dumps(
            self.prepare_request_payload(context, next_page_token)
        )
        headers = {}

        auth = self.authenticator
        if auth:
            headers.update(auth.auth_headers or {})

        request = cast(
            requests.PreparedRequest,
            self.requests_session.prepare_request(
                requests.Request(
                    method=http_method,
                    url=url,
                    params=params,
                    headers=headers,
                    data=request_data,
                ),
            ),
        )
        return request

    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Any:

        if not previous_token:
            return 2

        all_matches = extract_jsonpath(self.records_jsonpath, response.json())
        first_match = next(iter(all_matches), None)
        recs = first_match

        if not recs:
            return None

        return previous_token + 1

    def prepare_request_payload(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Optional[dict]:

        page = next_page_token or 1

        payload = {
            "query": {
                "paging": {
                    "limit": self._page_size,
                    "offset": (page - 1) * self._page_size,
                },
            }
        }

        start_date = self.get_starting_replication_key_value(context)
        if not start_date:
            start_date = self._tap.config.get("start_date")

        if start_date and self.replication_key:
            start_date = parse(start_date).strftime("%Y-%m-%dT%H:%M:%S.000Z")
            date_filter = {self.replication_key: {"$gt": start_date}}
            self.filters.update(date_filter)

        if self.filters:
            payload["query"]["filter"] = json.dumps(self.filters)

        if self.replication_key:
            sort = [{self.replication_key: "desc"}]
            payload["query"]["sort"] = json.dumps(sort)

        return payload
